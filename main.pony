use "collections"

type Node is (TreeNode | Empty)

primitive Empty
  fun check(): I32 => 0

class TreeNode
  var item: I32
  var left: Node
  var right: Node

  new create(i: I32) =>
    item = i
    left = Empty
    right = Empty

  fun check(): I32 =>
    (left.check() - right.check()) + item

primitive FillTree
  fun apply(i: I32, depth: I32): TreeNode =>
    var queue = Array[TreeNode](((1 << depth) - 1).u64())
    var result = TreeNode(i)
    queue.push(result)

    var head: I32 = 0
    var nodes: I32 = 0
    var target: I32 = ((1 << depth) - 1) - 1
    var it: I32 = i

    while nodes < target do
      it = 1 << it

      try
        var n = queue(head.u64())
        head = head + 1
        var l = TreeNode(it - 1)
        var r = TreeNode(it)
        n.left = l
        n.right = r
        queue.push(l)
        queue.push(r)
      end

      nodes = nodes + 2
    end
    result

actor Main
  var env: Env

  new create(e: Env) =>
    env = e
    let n = try env.args(1).i32() else 6 end
    var maxDepth = if 6 > n then 6 else n end

    StretchMemory(env, maxDepth + 1)
    let ll = LongLived(env, maxDepth)

    for depth in Range[I32](4, maxDepth + 1, 2) do
      var iterations = 16 << (maxDepth - depth)
      HeavyWork(env, iterations, depth)
    end

    ll()

type Task is (StretchMemory | LongLived | HeavyWork)

actor StretchMemory
  new create(env: Env, stretchDepth: I32) =>
    env.out.print("Stretch: " + FillTree(0, stretchDepth).check().string())

actor LongLived
  var _env: Env
  var _tree: Node

  new create(env: Env, maxDepth: I32) =>
    _env = env
    _tree = FillTree(0, maxDepth)

  be apply() =>
    _env.out.print("Long lived: " + _tree.check().string())

actor HeavyWork
  var _check: I32 = 0
  var _iterations: I32
  var _depth: I32
  var _env: Env

  new create(env: Env, iterations: I32, depth: I32) =>
    _iterations = iterations
    _depth = depth
    _env = env

    for i in Range[I32](0, iterations) do
      this(i)
    end

    done()

  be apply(i: I32) =>
    _check = _check + FillTree(i, _depth).check() +
      FillTree(-i, _depth).check()

  be done() =>
    _env.out.print((_iterations << 1).string() + " trees of depth " +
      _depth.string() + " check: " + _check.string())
